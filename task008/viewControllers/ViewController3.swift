//
//  ViewController3.swift
//  task008
//
//  Created by Ира Тиунова on 22.11.2022.
//

import Foundation
import UIKit

class ViewController3: UIViewController {

	@IBOutlet weak var button: UIButton!

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}


	@IBAction func didPressBack(_ backButton: UIButton) {
		navigationController?.popViewController(animated: true)
	}
}
